﻿# CENtools

**CEN-tools** is an integrative tool that identifies the underlying context that is associated with the essentiality of a gene from large-scale genome-scale CRISPR screens.

### Available datasets for pre-defined contexts
#### Essentiality Data
-   SANGER:  [Project Score](https://score.depmap.sanger.ac.uk/downloads)
-   BROAD:  [DepMap project - v. 19Q2](https://depmap.org/portal/)
-   SANGER and BROAD INTEGRATED: [Pacini et al, 2020](https://www.biorxiv.org/content/10.1101/2020.05.22.110247v2) 

#### Mutation Data
-   SANGER: [Cell Model Passports]((https://cellmodelpassports.sanger.ac.uk/downloads))
-   BROAD: [DepMap project - v.19Q2](https://depmap.org/portal/)
- SANGER and BROAD INTEGRATED: [Cell Model Passports]((https://cellmodelpassports.sanger.ac.uk/downloads)) & [DepMap project - v.19Q2](https://depmap.org/portal/) 
-   Mutation annotation was done for COSMIC hotspots with [CCLE](https://portals.broadinstitute.org/ccle)  & for validated oncogenic mutations with [Cancer Genome Interpreter](https://www.cancergenomeinterpreter.org/home)

#### Expression Data
-   SANGER: [Cell Model Passports]((https://cellmodelpassports.sanger.ac.uk/downloads))
-   BROAD: [DepMap project - v.19Q2](https://depmap.org/portal/)
-  SANGER and BROAD INTEGRATED: [DepMap project - v.19Q3](https://depmap.org/portal/) 

### Scripts

The repository *"centools"* consists of three main steps:
1. Data Preparation - *centools.data_preparation*

	1.1. **curation (Python)** : This script included functions to call necessary data frames and lists 
	
	1.2. **objects (Python)** : This script included objects and functions for calling and deserialising the objects
2. Prediction - *centools.prediction*

	2.1. **LR (Python)** : This script is for constructing logistic regression model based on [BAGEL]([https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-016-1015-8](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-016-1015-8)) essential and non-essential genes, taking distributions of the predicted genes' probability for being essential, and lastly obtaining arrays of 20 elements for each gene representing the probability distribution for being essential which will be used in clustering step.
	
	2.2. **Clustering (R )** : This script included the K-Means clustering step for identification of core, context, rare context and non-essential genes. 
3. Analysis - *centools.analysis*

	3.1. **CEN_analysis (Python)**: This script includes the construction of the CEN with respect to the input with predefined project and contexts. In our analysis, we run all possible projects and contexts and create the [whole CEN](https://www.ebi.ac.uk/biostudies/files/S-BSST479/Whole_network_CEN-tools.csv) which can be downloaded by clicking or can be downloaded from [BioStudies]([https://www.ebi.ac.uk/biostudies/studies/S-BSST479](https://www.ebi.ac.uk/biostudies/studies/S-BSST479)). 

___
### Citation
If you find CEN-Tools useful, please cite:  

Sharma S*, Dincer C*, Weidemüller P, Wright GJ, Petsalaki E., *CEN-tools: An integrative platform to identify the ‘contexts’ of essential genes.*
___
### Contact

Contact us [here](mailto:cen-tools@googlegroups.com) for any problems or feedback on CEN-tools.
___
### Terms and conditions
 
 **CEN-tools**  is an integrative tool that identifies the underlying context that is associated with the essentiality of a gene from large-scale genome-scale CRISPR screens.

Copyright © 2020 EMBL- European Bioinformatics Institute

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Neither the institution name nor the name CEN-tools can be used to endorse or promote products derived from this software without prior written permission. For written permission, please contact  [petsalaki@ebi.ac.uk](mailto:petsalaki@ebi.ac.uk).

Products derived from this software may not be called CEN-tools nor may CEN-tools appear in their names without prior written permission of the developers.

You should have received a copy of the GNU General Public License along with this program. If not, see  [gnu.org/licenses/](http://www.gnu.org/licenses/).

You can also view the licence  [here.](http://www.gnu.org/licenses/lgpl-3.0.txt)
___
### Further Disclaimer

For policies regarding the underlying data, please also refer to:

-   [DepMap: terms and conditions](https://depmap.org/portal/terms/)
-   [Project Score: terms and conditions](https://score.depmap.sanger.ac.uk/documentation)


