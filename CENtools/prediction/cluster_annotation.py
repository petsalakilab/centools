"""
                                    ---------------------
                                     C E N  -  T O O L S

                                         Annotation
                                    ---------------------
                                    After Running RScript
"""
########################################################################################################################
########################################################################################################################
# IMPORTING #

from paths import path, raw_data_path, curated_data_path, object_path
from data_preparation.curation import BEG, BNEG, adam_core, adam_context, stem_core, known_core, only_stem, dede_core
from data_preparation.objects import Gene
from prediction.LR import CEN_Prediction, deserialisation_cen_gene, prediction_path
import necessary_packages
import pandas, pickle

prediction_path = path + "prediction_output/"

########################################################################################################################
########################################################################################################################
# Reading the results from R Clustering Script #

def extract_sil(project):
    sil_df = pandas.read_csv(prediction_path + project.upper() + "/" +
                             project.upper() + "_clusters_sil_scores.csv", index_col = 4)
    return sil_df

def extract_clusters(project):
    cluster_df = pandas.read_csv(prediction_path + project.upper() + "/" +
                                 project.upper() + "_Clusters.csv", index_col = 0)[["Cluster_name"]]

    sil_df = extract_sil(project)[["sil_width"]]

    cluster_sil_df = pandas.concat([cluster_df, sil_df], axis = 1)

    return cluster_sil_df


########################################################################################################################
########################################################################################################################
# CEN CLUSTER OBJECT SERIALISATION #

def import_cluster_info(project, bin_number):

    global object_path

    cluster_df = extract_clusters(project)
    sil_df = extract_sil(project)

    project_cen_obj = deserialisation_cen_gene(project)
    for gene in project_cen_obj.keys():
        obj = project_cen_obj[gene]
        if gene in cluster_df.index and gene in sil_df.index:
            obj.add_classification(cluster_df)
            obj.add_cluster_info(bin_number, sil_df, cluster_df)

    pickle.dump(project_cen_obj, open(object_path + project.upper() + "_CLUSTER_CEN_OBJ.pkl", "wb"))


#for prj in ["SANGER", "BROAD", "INTEGRATED"]:import_cluster_info(prj, 20)

########################################################################################################################
########################################################################################################################
# CEN CLUSTER OBJECT DE-SERIALISATION #

def deserialisation_cen_cluster(project):

    global object_path

    cen_cluster_obj = pickle.load(open(object_path + project.upper() + "_CLUSTER_CEN_OBJ.pkl", "rb"))

    return cen_cluster_obj
