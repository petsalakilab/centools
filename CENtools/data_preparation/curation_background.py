"""
                                    ---------------------
                                     C E N  -  T O O L S

                                      Data  Preparation
                                    ---------------------
"""
########################################################################################################################
########################################################################################################################
# IMPORTING #

# from paths import raw_data_path, curated_data_path, object_path
#### added by PW - 17.05.2021 ####
# Specify the paths to import them from the modules
path = "/Users/pweide/Library/Mobile Documents/com~apple~CloudDocs/Documents/PhD/EBI/WholeCellSignaling/CEN-tools/centools/"
raw_data_path = path + "data/raw_data_integratedpublished/"
curated_data_path = path + "data/curated_data_integratedpublished/"
object_path = path + "data/objects/integrated_biorxiv/"

integrated_folder = "integrated_biorxiv"
####

import pandas, networkx, pickle, numpy
from sklearn.preprocessing import quantile_transform

########################################################################################################################
########################################################################################################################
# GENE ANNOTATIONS #

# Dede et al.
"""
dede_cegv2 = set(pandas.read_csv(raw_data_path + "Dede_CEGv2.csv", index_col=0).index)
dede_new = set(pandas.read_csv(raw_data_path + "Dede_new_COREs.csv", index_col=0).index)
dede_sanger = set(pandas.read_csv(raw_data_path + "Dede_sanger_specific.csv", index_col=0).index)
dede_specific = set(pandas.read_csv(raw_data_path + "Dede_specific_essentials.csv", index_col=0).index)
dede_core = dede_cegv2.union(dede_new.union(dede_sanger.union(dede_specific)))
pandas.DataFrame(dede_core).to_csv(curated_data_path + "Dede_etal_Core_genes.csv")
"""

########################################################################################################################
########################################################################################################################
# ESSENTIALITY #

"""
# Read the raw CRISPRcleanR corrected logFCs
# Individual studies
sanger_cor_fc_essentiality = pandas.read_csv(raw_data_path + "sanger/sanger_corrected_logFCs.tsv", sep='\t', index_col=0)
# Col : 325 / Row : 17995

broad_cor_fc_essentiality = pandas.read_csv(raw_data_path + "broad_19Q2/broad_corrected_logFCs_19Q2.tsv", sep='\t', index_col=0)
# Col : 489 / Row : 17343

# Take the common genes in both projects
common_genes_logfc = set(sanger_cor_fc_essentiality.index).intersection(set(broad_cor_fc_essentiality.index))
# Len : 16819

# Extract the logFCs of these common genes
sanger_cor_fc_essentiality = sanger_cor_fc_essentiality.loc[common_genes_logfc]
broad_cor_fc_essentiality = broad_cor_fc_essentiality.loc[common_genes_logfc]

# Write these files inside curated data folder:
sanger_cor_fc_essentiality.to_csv(curated_data_path + "sanger_cor_FC_essentiality.csv")
broad_cor_fc_essentiality.to_csv(curated_data_path + "broad_cor_FC_essentiality_19Q2.csv")

##################################################################

# Read the raw Scaled Bayesian Essentiality Tables
sanger_essentiality = pandas.read_csv(raw_data_path + "sanger/sanger_scaled_bayesian.tsv", sep='\t', index_col=0)
broad_essentiality = pandas.read_csv(raw_data_path + "broad_19Q2/broad_scaled_bayesian_19Q2.tsv", sep='\t', index_col=0)

# Take the common genes in essentiality tables of both projects:
common_genes = set(sanger_essentiality.index).intersection(set(broad_essentiality.index))

# Extract the essentiality values of these common genes in each project:
sanger_essentiality = sanger_essentiality.loc[common_genes]
broad_essentiality = broad_essentiality.loc[common_genes]

# Write these files inside curated data folder:
sanger_essentiality.to_csv(curated_data_path + "sanger_essentiality.csv")
broad_essentiality.to_csv(curated_data_path + "broad_essentiality.csv")
"""
##################################################################

# Integrated study
# integrated_cor_fc_essentiality = pandas.read_csv(raw_data_path + "integrated/integrated_corrected_logFCs.txt",sep = '\t',index_col=0)
integrated_cor_fc_essentiality = pandas.read_csv(raw_data_path + "integrated_folder/integrated_Sanger_Broad_essentiality_matrices_20200402/CRISPRcleanR_FC.txt", sep = '\t', index_col=0)
# Col : 786 / Row : 16827

# Since in integrated study, model names from Sanger project are in format of Sanger ID .
# From the Model info table from Cell Line Passport, the model IDs are changed to model names.
sanger_integrated_CLs = [i for i in integrated_cor_fc_essentiality.columns if i[:3] == "SID"]

# sanger_info = pandas.read_csv(raw_data_path + "sanger/sanger_model_list.csv", index_col = 0)
sanger_info = pandas.read_csv(raw_data_path + "sanger/model_list_20200610.csv", index_col = 0)
sanger_integrated_CLs_conv = {i: sanger_info.loc[i, "model_name"] for i in sanger_integrated_CLs
                              if i in sanger_info.index}

integrated_cor_fc_essentiality.rename(columns= {i: sanger_integrated_CLs_conv[i]
                                                for i in integrated_cor_fc_essentiality.columns
                                                if i[:3] == "SID" and i in sanger_integrated_CLs_conv.keys()},
                                      inplace=True)

integrated_cor_fc_essentiality.to_csv(curated_data_path + "integrated_cor_FC_essentiality.csv")

##################################################################

# Goncalves et al., 2020 scaling
# Function is retrieved from: https://github.com/EmanuelGoncalves/crispy

def scale(df, essential=BEG, non_essential=BNEG, metric=numpy.median):

    essential_metric = metric(df.reindex(essential).dropna(), axis=0)
    non_essential_metric = metric(df.reindex(non_essential).dropna(), axis=0)

    df = df.subtract(non_essential_metric).divide(non_essential_metric - essential_metric)
    df = df * -1

    return df


def quan_normalisation(df):

    # Sort each column (each cell line) independently

    df_sorted = pandas.DataFrame(
        numpy.sort(df.values, axis=0), index=df.index,columns=df.columns)

    # Calculate average of each observartion
    df_mean = df_sorted.mean(axis=1)

    # Rank sorting from 1
    df_mean.index = numpy.arange(1, len(df_mean) + 1)

    # Get rank of the original data and make the normalised data

    df_qn = df.rank(method="min").stack().astype(int).map(df_mean).unstack()

    return (df_qn)

# Quantile Normalised

# sanger_norm_fc = quan_normalisation(sanger_cor_fc_essentiality)
# broad_norm_fc  = quan_normalisation(broad_cor_fc_essentiality)
integrated_norm_fc  = quan_normalisation(integrated_cor_fc_essentiality)

# Check normalisation

# sanger_quan_cor_fc_essentiality = pandas.read_csv(raw_data_path + "sanger/sanger_qnorm_corrected_logFCs.tsv", sep='\t', index_col=0)
# broad_quan_cor_fc_essentiality = pandas.read_csv(raw_data_path + "broad_19Q2/broad_qnorm_corrected_logFCs.tsv", sep='\t', index_col=0)

# Scaling the normalised

# scaled_sanger_norm_fc = scale(sanger_norm_fc)
# scaled_broad_norm_fc = scale(broad_norm_fc)
scaled_integrated_norm_fc = scale(integrated_norm_fc)

# scaled_sanger_norm_fc.to_csv(curated_data_path + "scaled_norm_sanger_cor_FC_essentiality.csv")
# scaled_broad_norm_fc.to_csv(curated_data_path + "scaled_norm_broad_cor_FC_essentiality.csv")
scaled_integrated_norm_fc.to_csv(curated_data_path + "scaled_norm_integrated_cor_FC_essentiality.csv")


# Coessentiality

"""
sanger_co_essentiality = essentiality("SANGER", "MANUEL").T.corr().to_csv(curated_data_path + "sanger_CO_essentiality_Qscaled.csv")
broad_co_essentiality = essentiality("BROAD", "MANUEL").T.corr().to_csv(curated_data_path + "broad_CO_essentiality_Qscaled.csv")
integrated_co_essentiality = essentiality("INTEGRATED", "MANUEL").T.corr().to_csv(curated_data_path + "integrated_CO_essentiality_Qscaled.csv")
"""
integrated_co_essentiality = scaled_integrated_norm_fc.T.corr().to_csv(curated_data_path + "integrated_CO_essentiality_Qscaled.csv")

########################################################################################################################
########################################################################################################################
# CELL LINE MAPPING AND DISEASE INFO #

"""
# All Cell Lines in all projects

all_broad_CLs = list(set(broad_essentiality.columns).union(
    set([i for i in integrated_cor_fc_essentiality.columns if i[:3] == "ACH"])))
# 602 total / 360 common / 129 are only in broad / 113 in only integrated


all_sanger_CLs = list(set(sanger_essentiality.columns).union(
    set([i for i in integrated_cor_fc_essentiality.columns if i[:3] != "ACH"])))
# 326 total / 312 common / 13 are only in sanger / 1 in only integrated

sanger_ID_name = {i: row.model_name for i, row in sanger_info.iterrows()}

all_sanger_IDs = [k for i in all_sanger_CLs for k,v in sanger_ID_name.items() if v == i]
# 324 total / 2 of the missings are HT-29v1.1, HT-29v1.0

##################################################################

# Read info tables of both project

sanger_info = pandas.read_csv(raw_data_path + "sanger/sanger_model_list.csv", index_col = 0)
broad_info_19Q2 = pandas.read_csv(raw_data_path + "broad_19Q2/CCLE_sample_info_19Q2.csv", index_col = 0)
broad_info_19Q3 = pandas.read_csv(raw_data_path + "broad_19Q3/CCLE_sample_info_19Q3.csv", index_col = 0)
# 19Q3 is included 19Q2 s0 continue with it

##################################################################


# Extract only the CLs that we have

sanger_info = sanger_info[(sanger_info.model_name.isin(all_sanger_CLs)) |
                          (sanger_info.BROAD_ID.isin(all_broad_CLs))]
# 776 total / 324 for Sanger not 326 since (HT-29v1.1, HT-29v1.0) 2 cell lines do not have information./
# 452 for Broad

broad_info = broad_info_19Q3[(broad_info_19Q3.index.isin(all_broad_CLs))|
                             (broad_info_19Q3.Sanger_model_ID.isin(all_sanger_IDs))]
# 788 total / 602 for Broad / 186 for Sanger

##################################################################


# Mapping from Sanger and Broad IDs from different info tables

sanger_mapping = sanger_info[["model_name", "BROAD_ID"]]
sanger_mapping["stripped_cell_line_name"] = sanger_mapping.apply(lambda x: "".join(x.model_name.split("-")).upper(), axis = 1)
broad_mapping = broad_info[["stripped_cell_line_name", "Sanger_model_ID"]]

##################################################################


# Extract the matched IDs

sanger_to_broad = {sanger:{"broad":row.BROAD_ID, "check": None} for sanger, row in sanger_mapping.iterrows()}
broad_to_sanger = {broad:{"sanger": row.Sanger_model_ID, "check": None} for broad, row in broad_mapping.iterrows()}


# Check if these two info tables are consistent with cell line IDs

for i,l in sanger_to_broad.items():
    if type(l["broad"]) != float and l["broad"] in broad_to_sanger.keys():
        if broad_to_sanger[l["broad"]]["sanger"] == i:
            l["check"] = True
            broad_to_sanger[l["broad"]]["check"] = True
        else:
            l["check"] = False
            broad_to_sanger[l["broad"]]["check"] = False

for i,l in broad_to_sanger.items():
    if type(l["sanger"]) != float and l["check"] == None:
        if l["sanger"] in sanger_to_broad.keys():
            if sanger_to_broad[l["sanger"]]["broad"] == i:
                l["check"] = True
                sanger_to_broad[l["sanger"]]["check"] = True
            else:
                l["check"] = False
                sanger_to_broad[l["sanger"]]["check"] = False


# Consistent cell lines from two projects **** 575 pairs ****

check_true = [{"sanger": i, "broad": l["broad"]} for i,l in sanger_to_broad.items() if l["check"] == True]
for i,l in broad_to_sanger.items():
    if l["check"] and {"broad": i, "sanger": l["sanger"]} not in check_true:
        check_true.append({"broad": i, "sanger": l["sanger"]})


# Construct sanger-broad cell line ID conversion data table - Primary

k = 0
check_true_dfs = []
for d in check_true:
    t = pandas.DataFrame.from_dict({k:d}, orient = "index")
    check_true_dfs.append(t)
    k += 1

check_true_df = pandas.concat(check_true_dfs, axis = 0)

##################################################################


# All SANGER IDs - 1634

# How many sanger id are mapped with broad ids **** 581 **** included sanger from broad

sanger_in_broad_mapping = set([l["sanger"] for i,l in broad_to_sanger.items() if type(l["sanger"]) != float])


# How many sanger ids are in consistent id list **** 324 **** all essentiality CLs

sanger_in_sanger_mapping = set(sanger_to_broad.keys())


# Total mapped sanger ids **** 1634 ****

sanger_all = sanger_in_sanger_mapping.union(sanger_in_broad_mapping)


# Consistent Matched Sanger IDs - **** 575 ****

matched_sanger = sanger_all.intersection(set(check_true_df.sanger))


# Unmatched Sanger IDs - **** 9 ****

unmatched_sanger = sanger_all.difference(set(check_true_df.sanger))


# Matched Sanger IDs with Essentiality Table - **** 316 ****

matched_sanger_ess = set(all_sanger_CLs).intersection(
    set(sanger_mapping[sanger_mapping.index.isin(matched_sanger)].model_name))


# Unmatched Sanger IDs with Essentiality Table - **** 8 ****

unmatched_sanger_ess = set(all_sanger_CLs).intersection(
    set(sanger_mapping[sanger_mapping.index.isin(unmatched_sanger)].model_name))


# No Info Sanger IDs with Essentiality Table -- **** 2 **** The above 2 Cell lines (HT-29v1.1, HT-29v1.0)

rest_sanger_essentiality = set(all_sanger_CLs).difference(
    matched_sanger_ess.union(unmatched_sanger_ess))


# ALL BROAD IDs - 1459

# How many broad id are mapped with sanger ids **** 1439 ****

broad_in_sanger_mapping = set([l["broad"] for i,l in sanger_to_broad.items() if type(l["broad"]) != float])


# How many broad ids are in consistent id list **** 788 ****

broad_in_broad_mapping = set(broad_to_sanger.keys())


# Total mapped sanger ids **** 1459 ****

broad_all = broad_in_broad_mapping.union(broad_in_sanger_mapping)


# Matched Broad IDs - **** 575 ****

matched_broad = broad_all.intersection(set(check_true_df.broad))


# Unmatched Broad IDs - **** 884 ****

unmatched_broad = broad_all.difference(set(check_true_df.broad))


# Matched Broad IDs with Essentiality Table - **** 394 ****

matched_broad_ess = set(all_broad_CLs).intersection(matched_broad)


# Unmatched Broad IDs with Essentiality Table - **** 208 ****

unmatched_broad_ess = set(all_broad_CLs).intersection(unmatched_broad)


# No Info Broad IDs with Essentiality Table - **** 0 ****

rest_broad_essentiality = set(all_broad_CLs).difference(
    matched_broad_ess.union(unmatched_broad_ess))


##################################################################

# Finding unmatched sanger and broad IDs in essentiality tables

# Sanger **** 8 ****

unmatched_sanger_ess_sanger_IDs = set(sanger_mapping[sanger_mapping.model_name.isin(unmatched_sanger_ess)].index)


# **** 5 **** Mapped

secondary_sanger_to_broad = [{"sanger":row.Sanger_model_ID, "broad": i} for i,row in broad_mapping[
    broad_mapping.Sanger_model_ID.isin(unmatched_sanger_ess_sanger_IDs)].iterrows() if row[
    "Sanger_model_ID" in sanger_mapping and sanger_mapping.loc[row["Sanger_model_ID"],"BROAD_ID"] == i]]

secondary_matched_sanger_ess_sanger_IDs = set([i["sanger"] for i in secondary_sanger_to_broad])
secondary_matched_sanger_ess = set(sanger_mapping.loc[secondary_matched_sanger_ess_sanger_IDs].model_name)


# **** 3 **** Left

unmatched_sanger_ess_last = unmatched_sanger_ess.difference(secondary_matched_sanger_ess)
unmatched_sanger_ess_sanger_IDs_last = set(sanger_mapping[sanger_mapping.model_name.isin(unmatched_sanger_ess_last)].index)


# Broad **** 193 ****

secondary_broad_to_sanger = []
for i, row in sanger_mapping[sanger_mapping.BROAD_ID.isin(unmatched_broad_ess)].iterrows():
    if row.BROAD_ID in broad_mapping.index:
        if type(broad_mapping.loc[row.BROAD_ID, "Sanger_model_ID"]) != float and broad_mapping.loc[row.BROAD_ID, "Sanger_model_ID"] == i:
            secondary_broad_to_sanger.append({"sanger": i, "broad": row.BROAD_ID})
        elif type(broad_mapping.loc[row.BROAD_ID, "Sanger_model_ID"]) == float:
            secondary_broad_to_sanger.append({"sanger": i, "broad": row.BROAD_ID})

secondary_matched_broad_ess_broad_IDs = [i["broad"] for i in secondary_broad_to_sanger]


# **** 15 **** Left

unmatched_broad_ess_broad_IDs_last = unmatched_broad_ess.difference(secondary_matched_broad_ess_broad_IDs)


# Construct sanger-broad cell line ID conversion data table - Secondary **** 198 pairs ****

secondary_check = secondary_broad_to_sanger + secondary_sanger_to_broad

k = 0
secondary_check_dfs = []
for d in secondary_check:
    t = pandas.DataFrame.from_dict({k:d}, orient = "index")
    secondary_check_dfs.append(t)
    k += 1

secondary_check_df = pandas.concat(secondary_check_dfs, axis = 0)

##################################################################


# Sanger-broad cell line ID conversion data table - Primary + Secondary **** 773 ****

common_df = pandas.concat([check_true_df, secondary_check_df], axis = 0)

common_df["sanger_model_name"] = common_df.apply(lambda x: sanger_mapping.loc[x.sanger, "model_name"] if x.sanger in sanger_mapping.index and type(sanger_mapping.loc[x.sanger, "model_name"]) != float else "None", axis = 1)
common_df["broad_model_name"] = common_df.apply(lambda x: broad_mapping.loc[x.broad, "stripped_cell_line_name"] if x.broad in broad_mapping.index and type(broad_mapping.loc[x.broad, "stripped_cell_line_name"]) != float else "None", axis = 1)

ess_common_df = common_df[(common_df.sanger_model_name.isin(set(all_sanger_CLs)) |
                           (common_df.broad.isin(all_broad_CLs)))]

##################################################################


# Add unmatched and uninfo sanger and broad ids to the table with None

unmatched_sanger_final = [[i, "None", row.model_name, "None"] for i, row in sanger_mapping.loc[unmatched_sanger_ess_sanger_IDs_last].iterrows()]
df1 = pandas.DataFrame(unmatched_sanger_final, columns=common_df.columns)

unmatched_broad_final = [["None", i, "None", row.stripped_cell_line_name] for i, row in broad_mapping.loc[unmatched_broad_ess_broad_IDs_last].iterrows()]
df2= pandas.DataFrame(unmatched_broad_final, columns=common_df.columns)

uninfo_sanger = [["None", "None", i, "None"] for i in rest_sanger_essentiality]
df3 = pandas.DataFrame(uninfo_sanger, columns=common_df.columns)

unmatched = pandas.concat([df1, df2, df3], axis =0)

##################################################################


# Mapping sanger and broad ids only in essentiality tables **** 793 ***

ess_common_df = ess_common_df.append(unmatched, ignore_index=True)

##################################################################


# Add disease info

ess_common_df["sanger_cancer"] = ess_common_df.apply(
    lambda x: sanger_info.loc[x.sanger, "cancer_type"] if type(x.sanger) != float and
                                                          x.sanger in sanger_info.index else None,axis = 1)

ess_common_df["sanger_cancer_detail"] = ess_common_df.apply(
    lambda x: sanger_info.loc[x.sanger, "cancer_type_detail"].split("('")[1].split("(")[0].strip() if type(x.sanger) != float and
                                                          x.sanger in sanger_info.index else None,axis = 1)


ess_common_df["sanger_site"] = ess_common_df.apply(
    lambda x: sanger_info.loc[x.sanger, "sample_site"] if type(x.sanger) != float and
                                                          x.sanger in sanger_info.index else None,axis = 1)

ess_common_df["broad_site"] = ess_common_df.apply(
    lambda x: broad_info.loc[x.broad, "sample_collection_site"] if type(x.broad) != float and
                                                                   x.broad in broad_info.index else None,axis = 1)

ess_common_df["broad_cancer"] = ess_common_df.apply(
    lambda x: broad_info.loc[x.broad, "disease"] if type(x.broad) != float and
                                                    x.broad in broad_info.index else None,axis = 1)

ess_common_df["broad_cancer_detail"] = ess_common_df.apply(
    lambda x: broad_info.loc[x.broad, "disease_subtype"] if type(x.broad) != float and
                                                            x.broad in broad_info.index else None,axis = 1)

ess_common_df["broad_lineage"] = ess_common_df.apply(
    lambda x: broad_info.loc[x.broad, "lineage_subtype"] if type(x.broad) != float and
                                                            x.broad in broad_info.index else None,axis = 1)

ess_common_df["broad_lineage_detail"] = ess_common_df.apply(
    lambda x: broad_info.loc[x.broad, "lineage_sub_subtype"] if type(x.broad) != float and
                                                                x.broad in broad_info.index else None,axis = 1)

##################################################################


# Write the file inside curated data

ess_common_df.to_csv(curated_data_path + "Model_ID_Conversion.csv")

# Manuel curation on Model_ID_Conversion
# 4 cell lines changed to bone tissue by looking their expression clusters by Cell Line Selector
# Saved as mapping.csv which will be read in the script!
"""


########################################################################################################################
########################################################################################################################
# MUTATION INFO #

# Cancer Genome Interpreter Validated Oncogenic Mutations
"""
# Take information for oncogenic genes/mutations from Cancer Genome Interpreter

cgi = pandas.read_csv(raw_data_path + "catalog_of_validated_oncogenic_mutations.tsv",
                      sep = "\t", index_col = 0)[["protein"]]
cgi = cgi[cgi.protein  != "."]
cgi["Mutation"] = cgi.apply(lambda x: x.name + " " + x.protein, axis = 1)
oncogenic_genes = set(cgi.index)
oncogenic_mutations = set(cgi.Mutation)
"""

# Individual studies
# BROAD #
"""
# Read the raw broad mutation data from CCLE
broad_mutation = pandas.read_csv(raw_data_path + "broad_19Q2/CCLE_mutation_19Q2_raw.csv", index_col = 0)[[
    "DepMap_ID", "Hugo_Symbol", "Protein_Change", "isCOSMIChotspot"]]

# Extract only the cell lines having essentiality values
broad_essentiality = essentiality("BROAD", "BAYESIAN")
broad_mutation = broad_mutation[broad_mutation.DepMap_ID.isin(broad_essentiality.columns)]

# Extract only the genes having essentiality values
broad_mutation = broad_mutation[broad_mutation.Hugo_Symbol.isin(broad_essentiality.index)]

# Label the protein change due to mutation - (gene p.change)
broad_mutation.loc[list(broad_mutation[broad_mutation.Protein_Change.isna()].index), "Protein_Change"] = "p.?"
broad_mutation["Mutation"] = broad_mutation.apply(lambda x: x["Hugo_Symbol"] + " " + x["Protein_Change"], axis = 1)

# Manage index and columns
broad_mutation = broad_mutation.set_index(["DepMap_ID"])
broad_mutation.columns = ["Gene", "Protein_Change", "HotSpot", "Mutation"]

# Add information for oncogenic genes/mutations from Cancer Genome Interpreter
broad_mutation["Oncogenic_Mutation"] = broad_mutation.apply(lambda x: True if x.Mutation in oncogenic_mutations else False, axis = 1)
broad_mutation["Oncogenic_Gene"] = broad_mutation.apply(lambda x: True if x.Gene in oncogenic_genes else False, axis = 1)
broad_mutation = broad_mutation[["Mutation", "HotSpot", "Oncogenic_Mutation", "Oncogenic_Gene"]]

# Write the file into curated data folder
broad_mutation.to_csv(curated_data_path + "broad_19Q2_mutation.csv")
"""

# SANGER #
"""
# Read the raw sanger mutation data from cell line passports
sanger_mutation = pandas.read_csv(raw_data_path + "sanger/sanger_mutations_raw.csv")

# Extract only the cell lines having essentiality values
sanger_essentiality = essentiality("SANGER", "BAYESIAN")
sanger_mutation = sanger_mutation[sanger_mutation.model_name.isin(list(sanger_essentiality.columns))]

# Extract only the genes having essentiality values
sanger_mutation = sanger_mutation[sanger_mutation.gene_symbol.isin(sanger_essentiality.index)]

# Label the protein change due to mutation - (gene p.change)
sanger_mutation = sanger_mutation[sanger_mutation.protein_mutation != "p.?"]
sanger_mutation = sanger_mutation[sanger_mutation.protein_mutation != "-"]
sanger_mutation["Mutation"] = sanger_mutation.apply(lambda x : x["gene_symbol"] + " " + x["protein_mutation"], axis = 1)

# Find Sanger IDs corresponding to the model names

model_conversion = model_ID_conversion()
sanger_mutation["SANGER"] = sanger_mutation.apply(lambda x: model_conversion[
    model_conversion.sanger_model_name == x.model_name].sanger.values[0] if x.model_name in list(
    model_conversion.sanger_model_name) else "None", axis = 1)

# Manage index and columns
sanger_mutation = sanger_mutation.set_index(["SANGER"])
sanger_mutation = sanger_mutation[["model_name", "gene_symbol", "Mutation"]]

# Add information for oncogenic genes/mutations from Cancer Genome Interpreter
sanger_mutation["Oncogenic_Mutation"] = sanger_mutation.apply(lambda x: True if x.Mutation in oncogenic_mutations else False, axis = 1)
sanger_mutation["Oncogenic_Gene"] = sanger_mutation.apply(lambda x: True if x.gene_symbol in oncogenic_genes else False, axis = 1)

# Add Broad HotSpot information

raw_broad_mutations_2 = pandas.read_csv(raw_data_path + "broad_19Q2/CCLE_mutation_19Q2_raw.csv", index_col = 0)[[
    "DepMap_ID", "Hugo_Symbol", "Protein_Change", "isCOSMIChotspot"]]

raw_broad_mutations_3 = pandas.read_csv(raw_data_path + "broad_19Q3/CCLE_mutation_19Q3_raw.csv")[[
    "DepMap_ID", "Hugo_Symbol", "Protein_Change", "isCOSMIChotspot"]]

raw_broad_mutations = pandas.concat([raw_broad_mutations_2, raw_broad_mutations_3])
raw_broad_mutations = raw_broad_mutations.drop_duplicates()

# Add matching Sanger IDs to broad mutation data table
raw_broad_mutations["SANGER"] = raw_broad_mutations.apply(lambda x: list(
    model_conversion[model_conversion.broad == x.DepMap_ID].sanger.values)[0] if x.DepMap_ID in list(
    model_conversion.broad.values) and list(
    model_conversion[model_conversion.broad == x.DepMap_ID].sanger.values)[0] != "None" else "None", axis = 1)

raw_broad_mutations.loc[list(raw_broad_mutations[raw_broad_mutations.Protein_Change.isna()].index), "Protein_Change"] = "p.?"
raw_broad_mutations["Mutation"] = raw_broad_mutations.apply(lambda x: x["Hugo_Symbol"] + " " + x["Protein_Change"], axis = 1)

sanger_hotspots = raw_broad_mutations[(raw_broad_mutations.isCOSMIChotspot) & (
        raw_broad_mutations.SANGER != 'None')][["SANGER", "Mutation"]]

sanger_mutation["HotSpot"] = sanger_mutation.apply(lambda x: True if x.name in list(
    sanger_hotspots.SANGER.values) and x.Mutation in list(
    sanger_hotspots[sanger_hotspots.SANGER == x. name].Mutation) else False, axis = 1)

# Manage columns
sanger_mutation = sanger_mutation[["model_name", "Mutation", "HotSpot", "Oncogenic_Mutation", "Oncogenic_Gene"]]
sanger_mutation.columns = ["Model_Name", "Mutation", "HotSpot",  "Oncogenic_Mutation", "Oncogenic_Gene"]

# Write the file into curated data folder
sanger_mutation.to_csv(curated_data_path + "sanger_mutation.csv")
"""

# Integrated study

# BROAD 19Q3 in INTEGRATED
"""
# Read the raw broad mutation data from CCLE
int_broad_mutation = pandas.read_csv(raw_data_path + "broad_19Q3/CCLE_mutation_19Q3_raw.csv")[[
    "DepMap_ID", "Hugo_Symbol", "Protein_Change", "isCOSMIChotspot"]]

# Extract only the cell lines having essentiality values
integ_essentiality = essentiality("INTEGRATED", "FC")
int_broad_mutation = int_broad_mutation[int_broad_mutation.DepMap_ID.isin(integ_essentiality.columns)]

# Extract only the genes having essentiality values
int_broad_mutation = int_broad_mutation[int_broad_mutation.Hugo_Symbol.isin(integ_essentiality.index)]

# Label the protein change due to mutation - (gene p.change)
int_broad_mutation.loc[list(int_broad_mutation[int_broad_mutation.Protein_Change.isna()].index), "Protein_Change"] = "p.?"
int_broad_mutation["Mutation"] = int_broad_mutation.apply(lambda x: x["Hugo_Symbol"] + " " + x["Protein_Change"], axis = 1)

# Manage index and columns
int_broad_mutation = int_broad_mutation.set_index(["DepMap_ID"])
int_broad_mutation.columns = ["Gene", "Protein_Change", "HotSpot", "Mutation"]

# Add information for oncogenic genes/mutations from Cancer Genome Interpreter
int_broad_mutation["Oncogenic_Mutation"] = int_broad_mutation.apply(lambda x: True if x.Mutation in oncogenic_mutations else False, axis = 1)
int_broad_mutation["Oncogenic_Gene"] = int_broad_mutation.apply(lambda x: True if x.Gene in oncogenic_genes else False, axis = 1)
int_broad_mutation = int_broad_mutation[["Mutation", "HotSpot", "Oncogenic_Mutation", "Oncogenic_Gene"]]

# Write the file into curated data folder
int_broad_mutation.to_csv(curated_data_path + "broad_19Q3_mutation.csv")
"""

# SANGER in INTEGRATED
"""
# Read the raw sanger mutation data from cell line passports
int_sanger_mutation = pandas.read_csv(raw_data_path + "sanger/sanger_mutations_raw.csv")

# Extract only the cell lines having essentiality values

int_sanger_mutation = int_sanger_mutation[int_sanger_mutation.model_name.isin(list(integ_essentiality.columns))]

# Extract only the genes having essentiality values
int_sanger_mutation = int_sanger_mutation[int_sanger_mutation.gene_symbol.isin(integ_essentiality.index)]

# Label the protein change due to mutation - (gene p.change)
int_sanger_mutation = int_sanger_mutation[int_sanger_mutation.protein_mutation != "p.?"]
int_sanger_mutation = int_sanger_mutation[int_sanger_mutation.protein_mutation != "-"]
int_sanger_mutation["Mutation"] = int_sanger_mutation.apply(lambda x : x["gene_symbol"] + " " + x["protein_mutation"], axis = 1)

# Find Sanger IDs corresponding to the model names

int_sanger_mutation["SANGER"] = int_sanger_mutation.apply(lambda x: model_conversion[
    model_conversion.sanger_model_name == x.model_name].sanger.values[0] if x.model_name in list(
    model_conversion.sanger_model_name) else "None", axis = 1)

# Manage index and columns
int_sanger_mutation = int_sanger_mutation.set_index(["SANGER"])
int_sanger_mutation = int_sanger_mutation[["model_name", "gene_symbol", "Mutation"]]

# Add information for oncogenic genes/mutations from Cancer Genome Interpreter
int_sanger_mutation["Oncogenic_Mutation"] = int_sanger_mutation.apply(lambda x: True if x.Mutation in oncogenic_mutations else False, axis = 1)
int_sanger_mutation["Oncogenic_Gene"] = int_sanger_mutation.apply(lambda x: True if x.gene_symbol in oncogenic_genes else False, axis = 1)

# Add Broad HotSpot information

int_sanger_mutation["HotSpot"] = int_sanger_mutation.apply(lambda x: True if x.name in list(
    sanger_hotspots.SANGER.values) and x.Mutation in list(
    sanger_hotspots[sanger_hotspots.SANGER == x. name].Mutation) else False, axis = 1)

# Manage columns
int_sanger_mutation = int_sanger_mutation[["model_name", "Mutation", "HotSpot", "Oncogenic_Mutation", "Oncogenic_Gene"]]
int_sanger_mutation.columns = ["Model_Name", "Mutation", "HotSpot",  "Oncogenic_Mutation", "Oncogenic_Gene"]

# Write the file into curated data folder
int_sanger_mutation.to_csv(curated_data_path + "int_sanger_mutation.csv")
"""

# INTEGRATED
"""
int_broad, int_sanger = int_broad_mutation.reset_index(), int_sanger_mutation.reset_index()
integrated_mutation = pandas.concat([int_broad, int_sanger])
integrated_mutation = integrated_mutation.drop_duplicates()

integrated_mutation.to_csv(curated_data_path + "integrated_mutation.csv")
"""


########################################################################################################################
########################################################################################################################
#  EXPRESSION - FPKM/TPM INFO #

# BROAD #
"""
# Read the raw broad expression data from CCLE
broad_expression = pandas.read_csv(raw_data_path + "broad_19Q2/CCLE_expression_19Q2_raw.csv", index_col=0)

# Manage gene names
broad_expression = broad_expression.T
broad_expression["Genes"] = broad_expression.apply(lambda x : x.name.split(" (")[0], axis=1)

# Extract non-empty rows
broad_expression = broad_expression.dropna().reset_index()

# Manage indices
broad_expression = broad_expression.set_index("Genes")
broad_expression = broad_expression.drop(columns=["index"])

# Essentiality info
broad_essentiality = essentiality("BROAD", "BAYESIAN")

# Extract only the genes having essentiality values
broad_expression = broad_expression[broad_expression.index.isin(list(broad_essentiality.index))]

# Extract only the cell lines having essentiality values
broad_expression = broad_expression[list(set(broad_expression.columns).intersection(
    set(broad_essentiality.columns)))]


# For the genes having multiple tpm values --> summation -(only one gene)
for g in set(broad_expression.index):
    if type(broad_expression.loc[g]) == pandas.DataFrame:
        summations = {}
        for c in broad_expression.loc[g].columns:
            summations[c] = {g: sum(broad_expression.loc[g, c])}

        broad_expression = broad_expression.drop([g])
        df2 = pandas.DataFrame.from_dict(summations)
        broad_expression = broad_expression.append(df2)

# Write the file into curated data folder
broad_expression.to_csv(curated_data_path + "broad_19Q2_expression.csv")
"""

# SANGER #
"""
# Read gene annotation file from Cell Line Passports
gene = pandas.read_csv(raw_data_path + "sanger/sanger_gene_identifiers.csv", index_col = 0)[[
    "hgnc_symbol", "ensembl_gene_id"]]

# Read FPKM values from Cell Line Passports
sanger_expression_raw = pandas.read_csv(raw_data_path + "sanger/sanger_rnaseq_raw.csv")

# Log10 of FPKM with a pseudo count 1
sanger_expression_raw["log(fpkm+1)"] = sanger_expression_raw.apply(lambda x: numpy.log10(x.fpkm+1), axis = 1)

# Manage the columns
sanger_expression_raw = sanger_expression_raw[["gene_id", "model_id", "log(fpkm+1)"]]

# Take mean of FPKMs of the duplicated genes
new_sanger_exp = sanger_expression_raw.groupby(["gene_id", "model_id"]).mean().reset_index()

# Arrange data frame (index as gene/ column as cell line)
new_sanger_exp2 = new_sanger_exp.pivot(index = "gene_id", columns = "model_id", values = "log(fpkm+1)")

# SANGER Gene ID to HGNC Gene Symbols
new_sanger_exp2["Gene_names"] = [gene.loc[g, "hgnc_symbol"] if g in list(gene.index) else numpy.nan
                                 for g in list(new_sanger_exp2.index)]

# Essentiality information
sanger_essentiality = essentiality("SANGER", "BAYESIAN")

# Extract only the genes having essentiality values
new_sanger_exp2 = new_sanger_exp2[new_sanger_exp2.Gene_names.isin(sanger_essentiality.index)]

# Arrange indices
new_sanger_exp2 = new_sanger_exp2.set_index(["Gene_names"])

# SANGER Model ID to Cell Line Names
texp = new_sanger_exp2.T

sanger_map = sanger_mapping()

texp["model_names"] = texp.apply(lambda x: sanger_map.loc[
    x.name, "model_name"] if x.name in list(sanger_map.index) else "None", axis = 1)

# Eliminate cell lines having no model name
texp = texp[texp.model_names != "None"]

# Arrange columns
texp = texp.set_index(["model_names"])
new_sanger_exp3 = texp.T

# Extract only the cell lines having essentiality values
new_sanger_exp3 = new_sanger_exp3[[i for i in list(sanger_essentiality.columns) if i in new_sanger_exp3.columns]]

# Write the curated file
new_sanger_exp3.to_csv(curated_data_path + "sanger_expression.csv")
"""

# INTEGRATED #
"""
# BROAD 19Q3 in INTEGRATED
# Read the raw broad expression data from CCLE
int_broad_expression = pandas.read_csv(raw_data_path + "broad_19Q3/CCLE_expression_19Q3_raw.csv", index_col=0)

# Manage gene names
int_broad_expression = int_broad_expression.T
int_broad_expression["Genes"] = int_broad_expression.apply(lambda x : x.name.split(" (")[0], axis=1)

# Extract non-empty rows
int_broad_expression = int_broad_expression.dropna().reset_index()

# Manage indices
int_broad_expression = int_broad_expression.set_index("Genes")
int_broad_expression = int_broad_expression.drop(columns=["index"])

# Essentiality info
int_essentiality = essentiality("INTEGRATED", "FC")

# Extract only the genes having essentiality values
int_broad_expression = int_broad_expression[int_broad_expression.index.isin(list(int_essentiality.index))]

# Extract only the cell lines having essentiality values
int_broad_expression = int_broad_expression[list(set(int_broad_expression.columns).intersection(
    set(int_essentiality.columns)))]

# SANGER in INTEGRATED
# Read gene annotation file from Cell Line Passports
gene = pandas.read_csv(raw_data_path + "sanger/sanger_gene_identifiers.csv", index_col = 0)[[
    "hgnc_symbol", "ensembl_gene_id"]]

# Read FPKM values from Cell Line Passports
int_sanger_expression_raw = pandas.read_csv(raw_data_path + "sanger/sanger_rnaseq_raw.csv")

# Log10 of FPKM with a pseudo count 1
int_sanger_expression_raw["log(fpkm+1)"] = int_sanger_expression_raw.apply(lambda x: numpy.log10(x.fpkm+1), axis = 1)

# Manage the columns
int_sanger_expression_raw = int_sanger_expression_raw[["gene_id", "model_id", "log(fpkm+1)"]]

# Take mean of FPKMs of the duplicated genes
int_new_sanger_exp = int_sanger_expression_raw.groupby(["gene_id", "model_id"]).mean().reset_index()

# Arrange data frame (index as gene/ column as cell line)
int_new_sanger_exp2 = int_new_sanger_exp.pivot(index = "gene_id", columns = "model_id", values = "log(fpkm+1)")

# SANGER Gene ID to HGNC Gene Symbols
int_new_sanger_exp2["Gene_names"] = [gene.loc[g, "hgnc_symbol"] if g in list(gene.index) else numpy.nan
                                     for g in list(int_new_sanger_exp2.index)]

# Essentiality information
int_essentiality = essentiality("INTEGRATED", "FC")

# Extract only the genes having essentiality values
int_new_sanger_exp2 = int_new_sanger_exp2[int_new_sanger_exp2.Gene_names.isin(int_essentiality.index)]

# Arrange indices
int_new_sanger_exp2 = int_new_sanger_exp2.set_index(["Gene_names"])

# SANGER Model ID to Cell Line Names
int_texp = int_new_sanger_exp2.T

sanger_map = sanger_mapping()

int_texp["model_names"] = int_texp.apply(lambda x: sanger_map.loc[x.name, "model_name"] if x.name in list(
    sanger_map.index) else "None", axis = 1)

# Eliminate cell lines having no model name
int_texp = int_texp[int_texp.model_names != "None"]

# Arrange columns
int_texp = int_texp.set_index(["model_names"])
int_new_sanger_exp3 = int_texp.T

# Extract only the cell lines having essentiality values
int_new_sanger_exp3 = int_new_sanger_exp3[[i for i in list(int_essentiality.columns) if i in int_new_sanger_exp3.columns]]

# Write the curated file
common_int_expression_genes = set(int_broad_expression.index).intersection(set(int_new_sanger_exp3.index))
elim_int_broad_expression = int_broad_expression[int_broad_expression.index.isin(common_int_expression_genes)]
elim_int_new_sanger_exp3 = int_new_sanger_exp3[int_new_sanger_exp3.index.isin(common_int_expression_genes)]

elim_int_broad_expression.to_csv(curated_data_path + "broad_19Q3_expression.csv")
elim_int_new_sanger_exp3.to_csv(curated_data_path + "int_sanger_expression.csv")

# INTEGRATED

integrated_expression = pandas.concat([elim_int_broad_expression, elim_int_new_sanger_exp3], axis = 1)
integrated_expression.to_csv(curated_data_path + "integrated_expression.csv")
"""

# 20Q2 CCLE Expression
"""
# Read the raw broad expression data from CCLE
int_expression = pandas.read_csv(raw_data_path + "integrated/CCLE_expression_20Q2.csv", index_col=0)

# Manage gene names
int_expression = int_expression.T
int_expression["Genes"] = int_expression.apply(lambda x: x.name.split(" (")[0], axis=1)

# Extract non-empty rows
int_expression = int_expression.dropna().reset_index()

# Manage indices
int_expression = int_expression.set_index("Genes")
int_expression = int_expression.drop(columns=["index"])

# Essentiality info
int_essentiality = essentiality("INTEGRATED", "MANUEL")

# Model ID Conversion
conversion = model_ID_conversion()

# Extract only the genes having essentiality values
int_expression = int_expression[int_expression.index.isin(list(int_essentiality.index))]

# Extract non BROAD cell line names in essentiality table
sanger_int_CLs = [i for i in int_essentiality.columns if i[:3] != "ACH"]
broad_int_CLs = [i for i in int_essentiality.columns if i[:3] == "ACH"]
broad_IDs_sanger_int_CLs = {conversion[conversion.SANGER_NAME == i].BROAD.values[0]:i
                            for i in sanger_int_CLs if i in list(conversion.SANGER_NAME)}

# Change cell line name as in essentiality table

int_expression.rename(columns = broad_IDs_sanger_int_CLs, inplace=True)

# Extract only the cell lines having essentiality values
int_expression = int_expression[list(set(int_expression.columns).intersection(
    set(int_essentiality.columns)))]

int_expression.to_csv(curated_data_path + "integrated_expression.csv")
"""


########################################################################################################################
########################################################################################################################
# FUSION INFO #

# BROAD #
"""
# Read the raw data
broad_fusion = pandas.read_csv(raw_data_path + "broad_19Q2/CCLE_fusion_19Q2_raw.csv", index_col=0)[["LeftGene", "RightGene"]]

# Gene names have their Ensembl Gene IDs - so check the gne names and Ensembl IDs
# Download Biomart data for conversion
mart = pandas.read_csv(raw_data_path + "mart_ENSEMBL.txt", sep="\t")[["Gene stable ID", "Gene name"]].drop_duplicates()

# Extract official gene names
broad_fusion["5_Prime"] = broad_fusion.apply(lambda x: mart[
    mart["Gene stable ID"] == x.LeftGene.split(" (")[1].split(")")[0]][
    "Gene name"].values[0] if x.LeftGene.split(" (")[1].split(")")[0] in list(
    set(list(mart["Gene stable ID"]))) else None, axis=1)

broad_fusion["3_Prime"] = broad_fusion.apply(lambda x: mart[
    mart["Gene stable ID"] == x.RightGene.split(" (")[1].split(")")[0]][
    "Gene name"].values[0] if x.RightGene.split(" (")[1].split(")")[0] in list(
    set(list(mart["Gene stable ID"]))) else None, axis=1)

# Arrange columns
broad_fusion = broad_fusion[["3_Prime", "5_Prime"]]
broad_fusion = broad_fusion.dropna()

# Essentiality information
broad_essentiality = essentiality("BROAD", "BAYESIAN")

# Take cell lines having essentiality values
broad_fusion = broad_fusion[broad_fusion.index.isin(broad_essentiality.columns)]

# Take genes having essentiality values
broad_fusion = broad_fusion[(broad_fusion["5_Prime"].isin(broad_essentiality.index))&
                            (broad_fusion["3_Prime"].isin(broad_essentiality.index))]

# Drop duplicate info
broad_fusion = broad_fusion.drop_duplicates()

# Write curated data table
broad_fusion.to_csv(curated_data_path + "broad_19Q2_fusion.csv")
"""

# SANGER #
"""
# Read the raw data
sanger_fusion = pandas.read_csv(raw_data_path + "sanger/sanger_fusion_raw.csv")[[
    "model_name", "gene_symbol_3prime","gene_symbol_5prime"]]

# Arrange index
sanger_fusion = sanger_fusion.set_index(["model_name"])

# Arrange columns
sanger_fusion.columns = ["3_Prime", '5_Prime']

# Essentiality information
sanger_essentiality = essentiality("SANGER", "BAYESIAN")

# Take cell lines having essentiality values
sanger_fusion = sanger_fusion[sanger_fusion.index.isin(sanger_essentiality.columns)]

# Take genes having essentiality values
sanger_fusion = sanger_fusion[(sanger_fusion["5_Prime"].isin(sanger_essentiality.index))&
                              (sanger_fusion["3_Prime"].isin(sanger_essentiality.index))]

# Drop duplicate info
sanger_fusion = sanger_fusion.drop_duplicates()

# Write curated data table
sanger_fusion.to_csv(curated_data_path + "sanger_fusion.csv")
"""

# INTEGRATED
"""
# BROAD 19Q3 in INTEGRATED

# Read the raw data
int_broad_fusion = pandas.read_csv(raw_data_path + "broad_19Q3/CCLE_fusion_19Q3_raw.csv", index_col=0)[[
    "DepMap_ID", "LeftGene", "RightGene"]]

# Gene names have their Ensembl Gene IDs - so check the gne names and Ensembl IDs
# Download Biomart data for conversion
mart = pandas.read_csv(raw_data_path + "mart_ENSEMBL.txt", sep="\t")[["Gene stable ID", "Gene name"]].drop_duplicates()

# Extract official gene names
int_broad_fusion["5_Prime"] = int_broad_fusion.apply(lambda x: mart[
    mart["Gene stable ID"] == x.LeftGene.split(" (")[1].split(")")[0]][
    "Gene name"].values[0] if x.LeftGene.split(" (")[1].split(")")[0] in list(
    set(list(mart["Gene stable ID"]))) else None, axis=1)

int_broad_fusion["3_Prime"] = int_broad_fusion.apply(lambda x: mart[
    mart["Gene stable ID"] == x.RightGene.split(" (")[1].split(")")[0]][
    "Gene name"].values[0] if x.RightGene.split(" (")[1].split(")")[0] in list(
    set(list(mart["Gene stable ID"]))) else None, axis=1)

# Arrange indices
int_broad_fusion = int_broad_fusion.set_index(["DepMap_ID"])

# Arrange columns
int_broad_fusion = int_broad_fusion[["3_Prime", "5_Prime"]]
int_broad_fusion = int_broad_fusion.dropna()

# Essentiality information
int_essentiality = essentiality("INTEGRATED", "FC")

# Take cell lines having essentiality values
int_broad_fusion = int_broad_fusion[int_broad_fusion.index.isin(int_essentiality.columns)]

# Take genes having essentiality values
int_broad_fusion = int_broad_fusion[(int_broad_fusion["5_Prime"].isin(int_essentiality.index))&
                                    (int_broad_fusion["3_Prime"].isin(int_essentiality.index))]

# Drop duplicate info
int_broad_fusion = int_broad_fusion.drop_duplicates()

# Write curated data table
int_broad_fusion.to_csv(curated_data_path + "broad_19Q3_fusion.csv")


# SANGER in INTEGRATED

# Read the raw data
int_sanger_fusion = pandas.read_csv(raw_data_path + "sanger/sanger_fusion_raw.csv")[[
    "model_name", "gene_symbol_3prime","gene_symbol_5prime"]]

# Arrange index
int_sanger_fusion = int_sanger_fusion.set_index(["model_name"])

# Arrange columns
int_sanger_fusion.columns = ["3_Prime", '5_Prime']

# Essentiality information
int_essentiality = essentiality("INTEGRATED", "FC")

# Take cell lines having essentiality values
int_sanger_fusion = int_sanger_fusion[int_sanger_fusion.index.isin(int_essentiality.columns)]

# Take genes having essentiality values
int_sanger_fusion = int_sanger_fusion[(int_sanger_fusion["5_Prime"].isin(int_essentiality.index))&
                                      (int_sanger_fusion["3_Prime"].isin(int_essentiality.index))]

# Drop duplicate info
int_sanger_fusion = int_sanger_fusion.drop_duplicates()

# Write curated data table
int_sanger_fusion.to_csv(curated_data_path + "int_sanger_fusion.csv")

# INTEGRATED

integrated_fusion = pandas.concat([int_broad_fusion, int_sanger_fusion])
integrated_fusion.to_csv(curated_data_path + "integrated_fusion.csv")
"""


########################################################################################################################
########################################################################################################################
# CNV INFO #


# BROAD #
"""
# Read raw cnv data from CCLE
broad_cnv = pandas.read_csv(raw_data_path + "broad_19Q2/CCLE_cnv_19Q2_raw.csv", index_col = 0)

# Essentiality information
broad_essentiality = essentiality("BROAD", "BAYESIAN")

# Extract the cell lines having essentiality values
broad_cnv = broad_cnv[broad_cnv.index.isin(broad_essentiality.columns)]

# Manage gene names
broad_cnv = broad_cnv.T
broad_cnv["Genes"] = broad_cnv.apply(lambda x: x.name.split(" (")[0], axis=1)

# Manage indices
broad_cnv = broad_cnv.set_index(["Genes"])

# Extract the genes having essentiality values
broad_cnv = broad_cnv[broad_cnv.index.isin(broad_essentiality.index)]

broad_cnv.to_csv(curated_data_path + "broad_19Q2_cnv.csv")
"""

# SANGER #
"""
# Read raw cnv data from cell line passports
sanger_cnv = pandas.read_csv(raw_data_path + "sanger/sanger_cnv_raw.csv", index_col = 1)

# Essentiality information
sanger_essentiality = essentiality("SANGER", "FC")

# Extract the genes having essentiality values
sanger_cnv = sanger_cnv[sanger_cnv.index.isin(sanger_essentiality.index)]

# Extract the cell lines having essentiality values
# Use mapping to change sanger cell line ID to cell line name conversion
sanger_cnv = sanger_cnv.drop(columns=["model_id"])
tcnv = sanger_cnv.T
sanger_map = sanger_mapping()
tcnv["model_names"] = [sanger_map.loc[ind, "model_name"] if ind in list(sanger_map.index)
                       else "None" for ind in list(tcnv.index)]

# Eliminate if there is no matching cell line name
tcnv = tcnv[tcnv.model_names != "None"]

# Change cell line iD to cell line name
tcnv = tcnv.set_index(["model_names"])
sanger_cnv = tcnv.T

sanger_cnv = sanger_cnv[list(set(sanger_cnv.columns).intersection(set(sanger_essentiality.columns)))]

sanger_cnv.to_csv(curated_data_path + "sanger_cnv.csv")
"""

# INTEGRATED #
"""
# BROAD 19Q3 in INTEGRATED

# Read raw cnv data from CCLE
int_broad_cnv = pandas.read_csv(raw_data_path + "broad_19Q3/CCLE_cnv_19Q3_raw.csv", index_col = 0)

# Essentiality information
int_essentiality = essentiality("INTEGRATED", "FC")

# Extract the cell lines having essentiality values
int_broad_cnv = int_broad_cnv[int_broad_cnv.index.isin(int_essentiality.columns)]

# Manage gene names
int_broad_cnv = int_broad_cnv.T
int_broad_cnv["Genes"] = int_broad_cnv.apply(lambda x: x.name.split(" (")[0], axis=1)

# Manage indices
int_broad_cnv = int_broad_cnv.set_index(["Genes"])

# Extract the genes having essentiality values
int_broad_cnv = int_broad_cnv[int_broad_cnv.index.isin(int_essentiality.index)]

# SANGER in INTEGRATED

# Read raw cnv data from cell line passports
int_sanger_cnv = pandas.read_csv(raw_data_path + "sanger/sanger_cnv_raw.csv", index_col = 1)

# Essentiality information
int_essentiality = essentiality("INTEGRATED", "FC")

# Extract the genes having essentiality values
int_sanger_cnv = int_sanger_cnv[int_sanger_cnv.index.isin(int_essentiality.index)]

# Extract the cell lines having essentiality values
# Use mapping to change sanger cell line ID to cell line name conversion
int_sanger_cnv = int_sanger_cnv.drop(columns=["model_id"])
int_tcnv = int_sanger_cnv.T

sanger_map = sanger_mapping()

int_tcnv["model_names"] = [sanger_map.loc[ind, "model_name"] if ind in list(sanger_map.index)
                           else "None" for ind in list(int_tcnv.index)]

# Eliminate if there is no matching cell line name
int_tcnv = int_tcnv[int_tcnv.model_names != "None"]

# Change cell line iD to cell line name
int_tcnv = int_tcnv.set_index(["model_names"])
int_sanger_cnv = int_tcnv.T

int_sanger_cnv = int_sanger_cnv[list(set(int_sanger_cnv.columns).intersection(set(int_essentiality.columns)))]


# INTEGRATED

# Eliminate the genes including in both projects

common_cnv_genes = set(int_sanger_cnv.index).intersection(set(int_broad_cnv.index))
elim_int_sanger_cnv = int_sanger_cnv[int_sanger_cnv.index.isin(common_cnv_genes)]
elim_int_broad_cnv = int_broad_cnv[int_broad_cnv.index.isin(common_cnv_genes)]

# Write the files into curated data file folder
elim_int_sanger_cnv.to_csv(curated_data_path + "int_sanger_cnv.csv")
elim_int_broad_cnv.to_csv(curated_data_path + "broad_19Q3_cnv.csv")

# Integrate the files into a file
integrated_cnv = pandas.concat([elim_int_broad_cnv, elim_int_sanger_cnv], axis = 1)
integrated_cnv.to_csv(curated_data_path + "integrated_cnv.csv")
"""


########################################################################################################################
########################################################################################################################
# DRUG INFO #

# SANGER #
"""
# Read GDSC 1 and 2 data
drug1 = pandas.read_csv(raw_data_path + "sanger/GDSC1_fitted_dose_response_15Oct19.csv", index_col = 0)[[
    "CELL_LINE_NAME", "DRUG_NAME", "PUTATIVE_TARGET", "PATHWAY_NAME", 'COMPANY_ID', "LN_IC50","Z_SCORE"]]

drug2 = pandas.read_csv(raw_data_path + "sanger/GDSC2_fitted_dose_response_25Feb20.csv", index_col = 0)[[
    "CELL_LINE_NAME", "DRUG_NAME", "PUTATIVE_TARGET", "PATHWAY_NAME", 'COMPANY_ID', "LN_IC50","Z_SCORE"]]

# Essentiality Information
sanger_essentiality = essentiality("SANGER", "BAYESIAN")

# Find number of cell lines and putative targets both in raw data and curated
raw_drug1_cls = set(drug1.CELL_LINE_NAME)
raw_drug1_targets = set([i for i in drug1.PUTATIVE_TARGET if len(i.split(", "))== 1]+\
                        [k for i in drug1.PUTATIVE_TARGET if len(i.split(", "))> 1 for k in i.split(", ")])
sanger_drug1_cl = set([cl for cl in drug1.CELL_LINE_NAME if cl in set(sanger_essentiality.columns)])
sanger_drug1_targets = set([t for t in raw_drug1_targets if t in set(sanger_essentiality.index)])

raw_drug2_cls = set(drug2.CELL_LINE_NAME)
raw_drug2_targets = set([i for i in drug2.PUTATIVE_TARGET if type(i) != float and len(i.split(", "))== 1]+\
                        [k for i in drug2.PUTATIVE_TARGET if type(i) != float and len(i.split(", "))> 1 for k in i.split(", ")])
sanger_drug2_cl = set([cl for cl in drug2.CELL_LINE_NAME if cl in set(sanger_essentiality.columns)])
sanger_drug2_targets = set([t for t in raw_drug2_targets if t in set(sanger_essentiality.index)])

# Find IC50 values from ln(IC50) values
drug1["IC50"] = drug1.apply(lambda x : numpy.exp(x.LN_IC50), axis = 1)
drug2["IC50"] = drug2.apply(lambda x : numpy.exp(x.LN_IC50), axis = 1)

# Concat two data frames
drug = pandas.concat([drug1, drug2], axis = 0)

# Extract cell lines having essentiality values
sanger_drug = drug[drug.CELL_LINE_NAME.isin(set(sanger_essentiality.columns))]
sanger_drug = sanger_drug.dropna()

# Arrange columns
sanger_drug.columns = ["CELL_LINE", "DRUG_NAME", "PUTATIVE_TARGET", "PATHWAY_NAME", "COMPANY_ID", "LN_IC50", "Z_SCORE", "IC50"]

# Write curated drug data
sanger_drug.to_csv(curated_data_path + "sanger_drug.csv")
"""

# BROAD #
"""
# Read GDSC 1 and 2 data
drug1 = pandas.read_csv(raw_data_path + "SANGER/GDSC1_fitted_dose_response_15Oct19.csv", index_col = 0)[[
    "CELL_LINE_NAME", "DRUG_NAME", "PUTATIVE_TARGET", "PATHWAY_NAME", 'COMPANY_ID', "LN_IC50","Z_SCORE"]]

drug2 = pandas.read_csv(raw_data_path + "SANGER/GDSC2_fitted_dose_response_25Feb20.csv", index_col = 0)[[
    "CELL_LINE_NAME", "DRUG_NAME", "PUTATIVE_TARGET", "PATHWAY_NAME", 'COMPANY_ID', "LN_IC50","Z_SCORE"]]

# Find IC50 values from ln(IC50) values
drug1["IC50"] = drug1.apply(lambda x : numpy.exp(x.LN_IC50), axis = 1)
drug2["IC50"] = drug2.apply(lambda x : numpy.exp(x.LN_IC50), axis = 1)

# Concat two data frames
drug = pandas.concat([drug1, drug2], axis = 0)

# Find corresponding broad id for cell line names

model_conversion = model_ID_conversion()

drug_broad_ID = {}
for i in set(drug.CELL_LINE_NAME):
    cl = "".join(i.split("-"))
    if cl in model_conversion.broad_model_name.values:
        if i not in drug_broad_ID.keys():
            drug_broad_ID[i] = model_conversion[model_conversion.broad_model_name == cl]["broad"].values
        else:
            t = drug_broad_ID[i]
            if model_conversion[model_conversion.broad_model_name == cl]["broad"].values[0] not in t:
                t.append(model_conversion[model_conversion.broad_model_name == cl]["broad"].values[0])
            drug_broad_ID[i] = t
    elif cl in model_conversion.sanger_model_name.values:
        if i not in drug_broad_ID.keys():
            drug_broad_ID[i] = model_conversion[model_conversion.sanger_model_name == cl]["broad"].values
        else:
            t = drug_broad_ID[i]
            if model_conversion[model_conversion.sanger_model_name == cl]["broad"].values[0] not in t:
                t.append(model_conversion[model_conversion.sanger_model_name == cl]["broad"].values[0])
            drug_broad_ID[i] = t

# Put broad id column to the drug data frame
drug["BROAD"] = drug.apply(lambda x: drug_broad_ID[x.CELL_LINE_NAME][0] if x.CELL_LINE_NAME in drug_broad_ID.keys() else "None", axis=1)

# Arrange the columns
broad_drug = drug[["BROAD", "DRUG_NAME", "PUTATIVE_TARGET", "PATHWAY_NAME", "COMPANY_ID", "LN_IC50", "Z_SCORE", "IC50"]]

# Essentiality Information
broad_essentiality = essentiality("BROAD", "BAYESIAN")

# Extract cell lines having essentiality values
broad_drug = broad_drug[broad_drug.BROAD.isin(set(broad_essentiality.columns))]
broad_drug = broad_drug.dropna()

# Find number of cell lines and putative targets both in raw data and curated
raw_drug_target = set([i for i in drug.PUTATIVE_TARGET if type(i) != float and len(i.split(", "))== 1]+\
                      [k for i in drug.PUTATIVE_TARGET if type(i) != float and len(i.split(", "))> 1 for k in i.split(", ")])

broad_drug_targets = set([t for t in raw_drug_target if t in set(broad_essentiality.index)])

# Write curated data
broad_drug.to_csv(curated_data_path + "broad_19Q2_drug.csv")
"""

# INTEGRATED
"""
# Read GDSC 1 and 2 data

drug1 = pandas.read_csv(raw_data_path + "SANGER/GDSC1_fitted_dose_response_15Oct19.csv", index_col = 0)[[
    "CELL_LINE_NAME", "DRUG_NAME", "PUTATIVE_TARGET", "PATHWAY_NAME", 'COMPANY_ID', "LN_IC50","Z_SCORE"]]

drug2 = pandas.read_csv(raw_data_path + "SANGER/GDSC2_fitted_dose_response_25Feb20.csv", index_col = 0)[[
    "CELL_LINE_NAME", "DRUG_NAME", "PUTATIVE_TARGET", "PATHWAY_NAME", 'COMPANY_ID', "LN_IC50","Z_SCORE"]]

# Find IC50 values from ln(IC50) values
drug1["IC50"] = drug1.apply(lambda x : numpy.exp(x.LN_IC50), axis = 1)
drug2["IC50"] = drug2.apply(lambda x : numpy.exp(x.LN_IC50), axis = 1)

# Concat two data frames
int_drug = pandas.concat([drug1, drug2], axis = 0)

# BROAD 19Q3 in INTEGRATED

# Find corresponding broad id for cell line names

model_conversion = model_ID_conversion()

int_drug_broad_ID = {}
for i in set(int_drug.CELL_LINE_NAME):
    cl = "".join(i.split("-"))
    if cl in model_conversion.broad_model_name.values:
        if i not in int_drug_broad_ID.keys():
            int_drug_broad_ID[i] = model_conversion[model_conversion.broad_model_name == cl]["broad"].values
        else:
            t = int_drug_broad_ID[i]
            if model_conversion[model_conversion.broad_model_name == cl]["broad"].values[0] not in t:
                t.append(model_conversion[model_conversion.broad_model_name == cl]["broad"].values[0])
            int_drug_broad_ID[i] = t
    elif cl in model_conversion.sanger_model_name.values:
        if i not in int_drug_broad_ID.keys():
            int_drug_broad_ID[i] = model_conversion[model_conversion.sanger_model_name == cl]["broad"].values
        else:
            t = int_drug_broad_ID[i]
            if model_conversion[model_conversion.sanger_model_name == cl]["broad"].values[0] not in t:
                t.append(model_conversion[model_conversion.sanger_model_name == cl]["broad"].values[0])
            int_drug_broad_ID[i] = t

# Put broad id column to the drug data frame
int_drug["BROAD"] = drug.apply(lambda x: drug_broad_ID[x.CELL_LINE_NAME][0] if x.CELL_LINE_NAME in drug_broad_ID.keys() else "None", axis=1)

# Arrange the columns
int_broad_drug = int_drug[["BROAD", "DRUG_NAME", "PUTATIVE_TARGET", "PATHWAY_NAME", "COMPANY_ID", "LN_IC50", "Z_SCORE", "IC50"]]
int_broad_drug.columns = ["CELL_LINE", "DRUG_NAME", "PUTATIVE_TARGET", "PATHWAY_NAME", "COMPANY_ID", "LN_IC50", "Z_SCORE", "IC50"]

# Essentiality Information
int_essentiality = essentiality("INTEGRATED", "FC")

# Extract cell lines having essentiality values
int_broad_drug = int_broad_drug[int_broad_drug.CELL_LINE.isin(set(int_essentiality.columns))]
int_broad_drug = int_broad_drug.dropna()

# Find number of cell lines and putative targets both in raw data and curated
int_raw_drug_target = set([i for i in int_drug.PUTATIVE_TARGET if type(i) != float and len(i.split(", "))== 1]+\
                          [k for i in int_drug.PUTATIVE_TARGET if type(i) != float and len(i.split(", "))> 1 for k in i.split(", ")])

int_broad_drug_targets = set([t for t in int_raw_drug_target if t in set(int_essentiality.index)])

# Write curated data
int_broad_drug.to_csv(curated_data_path + "broad_19Q3_drug.csv")


# SANGER in INTEGRATED

# Read GDSC 1 and 2 data
drug1 = pandas.read_csv(raw_data_path + "sanger/GDSC1_fitted_dose_response_15Oct19.csv", index_col = 0)[[
    "CELL_LINE_NAME", "DRUG_NAME", "PUTATIVE_TARGET", "PATHWAY_NAME", 'COMPANY_ID', "LN_IC50","Z_SCORE"]]

drug2 = pandas.read_csv(raw_data_path + "sanger/GDSC2_fitted_dose_response_25Feb20.csv", index_col = 0)[[
    "CELL_LINE_NAME", "DRUG_NAME", "PUTATIVE_TARGET", "PATHWAY_NAME", 'COMPANY_ID', "LN_IC50","Z_SCORE"]]

# Essentiality Information
int_essentiality = essentiality("INTEGRATED", "FC")

# Find number of cell lines and putative targets both in raw data and curated
raw_drug1_cls = set(drug1.CELL_LINE_NAME)
raw_drug1_targets = set([i for i in drug1.PUTATIVE_TARGET if len(i.split(", "))== 1]+\
                        [k for i in drug1.PUTATIVE_TARGET if len(i.split(", "))> 1 for k in i.split(", ")])
sanger_drug1_cl = set([cl for cl in drug1.CELL_LINE_NAME if cl in set(int_essentiality.columns)])
sanger_drug1_targets = set([t for t in raw_drug1_targets if t in set(int_essentiality.index)])

raw_drug2_cls = set(drug2.CELL_LINE_NAME)
raw_drug2_targets = set([i for i in drug2.PUTATIVE_TARGET if type(i) != float and len(i.split(", "))== 1]+\
                        [k for i in drug2.PUTATIVE_TARGET if type(i) != float and len(i.split(", "))> 1 for k in i.split(", ")])
sanger_drug2_cl = set([cl for cl in drug2.CELL_LINE_NAME if cl in set(int_essentiality.columns)])
sanger_drug2_targets = set([t for t in raw_drug2_targets if t in set(int_essentiality.index)])

# Find IC50 values from ln(IC50) values
drug1["IC50"] = drug1.apply(lambda x : numpy.exp(x.LN_IC50), axis = 1)
drug2["IC50"] = drug2.apply(lambda x : numpy.exp(x.LN_IC50), axis = 1)

# Concat two data frames
int_drug = pandas.concat([drug1, drug2], axis = 0)

# Extract cell lines having essentiality values
int_sanger_drug = int_drug[int_drug.CELL_LINE_NAME.isin(set(int_essentiality.columns))]
int_sanger_drug = int_sanger_drug.dropna()

# Arrange columns
int_sanger_drug.columns = ["CELL_LINE", "DRUG_NAME", "PUTATIVE_TARGET", "PATHWAY_NAME", "COMPANY_ID", "LN_IC50", "Z_SCORE", "IC50"]

# Write curated drug data
int_sanger_drug.to_csv(curated_data_path + "int_sanger_drug.csv")

# INTEGRATED

integrated_drug = pandas.concat([int_broad_drug, int_sanger_drug])
integrated_drug.to_csv(curated_data_path + "integrated_drug.csv")
"""
